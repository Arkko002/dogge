-- Your SQL goes here
CREATE TABLE emails (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    date TEXT NOT NULL,
    'from' TEXT NOT NULL,
    subject TEXT NOT NULL,
    body TEXT NOT NULL
);
