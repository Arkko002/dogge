use crate::schema::emails::dsl::emails;
use diesel::prelude::*;
use diesel::result::Error;
use diesel::SqliteConnection;

use crate::models::Email;

pub struct EmailRepository {
    database_url: String,
}

impl EmailRepository {
    pub fn get_all(&self) -> Result<Vec<Email>, Error> {
        let connection = &mut self.establish_connection();
        emails.select(Email::as_select()).load(connection)
    }

    // pub fn insert_emails(&self, email_list: Vec<Email>) {
    //     let connection = &mut self.establish_connection();
    //
    //     diesel::insert_into(emails)
    //         .values(&email_list)
    //         .returning()
    //         .get_results(connection);
    // }

    fn establish_connection(&self) -> SqliteConnection {
        SqliteConnection::establish(&self.database_url)
            .unwrap_or_else(|_| panic!("Error connecting to {}", self.database_url))
    }
}
