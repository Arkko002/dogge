use configuration::{get_config, Settings};
use tui::{start_tui, App};

use crate::imap::fetch_imap_inbox_top;

mod configuration;
mod imap;
mod tui;
mod repository;
mod schema;
mod models;

fn main() {
    let config: Settings = get_config().unwrap();

    let emails = fetch_imap_inbox_top(&config.emails[0]).unwrap();

    let app = App::new(emails);
    let _ = start_tui(app);
    
}
