// @generated automatically by Diesel CLI.

diesel::table! {
    emails (id) {
        id -> Nullable<Integer>,
        date -> Text,
        from -> Text,
        subject -> Text,
        body -> Text,
    }
}
