use std::net::TcpStream;

use imap::{self, Session};
use mail_parser::Message;
use native_tls::TlsStream;

use crate::{configuration::EmailConfig, models::Email};

pub trait ImapEmailAccount {
    fn get_account_session(&self) -> imap::error::Result<Session<TlsStream<TcpStream>>>;
}

pub fn fetch_imap_inbox_top(account: &EmailConfig) -> imap::error::Result<Vec<Email>> {
    let mut imap_session = account.get_account_session()?;

    // we want to fetch the first email in the INBOX mailbox
    imap_session.select("INBOX")?;

    // fetch message number 1 in this mailbox, along with its RFC822 field.
    // RFC 822 dictates the format of the body of e-mails
    let messages = imap_session.fetch("1:10", "RFC822")?;

    let mut messages_parsed_string: Vec<String> = vec![];

    for message in messages.iter() {
        // extract the message's body
        let body = message.body().expect("message did not have a body!");
        let body = std::str::from_utf8(body)
            .expect("message was not valid utf-8")
            .to_string();

        messages_parsed_string.push(body)
    }

    // be nice to the server and log out
    imap_session.logout()?;

    // TODO:
    let mut emails_parsed: Vec<Email> = vec![];
    // for message_string in messages.iter() {
    //     let message_combined = message_string.body().unwrap()
    //     let message = Message::parse(message_string.body()).unwrap();
    //
    //     let email: Email = parse_message_to_email(message);
    //     emails_parsed.push(email);
    // }
    for message_string in messages_parsed_string.iter() {
        let message = Message::parse(message_string.as_bytes()).unwrap();

        let email: Email = parse_message_to_email(message);
        emails_parsed.push(email);
    }

    Ok(emails_parsed)
}

fn parse_message_to_email(message: Message) -> Email {
    let date = message.date().unwrap().to_rfc822();
    // let from = message.from().clone().unwrap_text().to_string();
    let subject = message.subject().unwrap().to_owned();
    let body = message
        .text_bodies()
        .map(|text_body| String::from_utf8_lossy(text_body.contents()))
        .collect();

    Email {
        id: None,
        date,
        from: "None".to_string(),
        subject,
        body,
    }
}
