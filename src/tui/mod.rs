use std::{
    error::Error,
    io::{self, Stdout},
    time::Duration,
};

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{
    prelude::{Backend, Constraint, CrosstermBackend, Direction, Layout},
    style::{Modifier, Style},
    text::{Line, Span},
    widgets::{Block, Borders, List, ListItem, ListState, Tabs},
    Frame, Terminal,
};
use statig::{state_machine, Response, superstate};

use crate::{configuration::EmailConfig, imap::fetch_imap_inbox_top, models::Email};

mod state;

struct StatefulList<T> {
    state: ListState,
    items: Vec<T>,
}

impl<T> StatefulList<T> {
    fn with_items(items: Vec<T>) -> StatefulList<T> {
        StatefulList {
            state: ListState::default(),
            items,
        }
    }

    fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    fn previous(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.items.len() - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    fn unselect(&mut self) {
        self.state.select(None);
    }

    fn selected(&mut self) -> Option<&T> {
        if let Some(selected_state) = self.state.selected() {
            return Some(&self.items[selected_state]);
        }

        None
    }
}





// TODO:
struct EmailAccountTab<'a> {
    tab_title: &'a str,
    emails: StatefulList<Email>,
    show_email_popup: bool,
}

// TODO: Consider describing state with enums or events instead of booleans
// TODO: Bundle email list with account index number
pub struct App<'a> {
    account_titles: Vec<&'a str>,
    account_index: Option<usize>,

    items: StatefulList<Email>,
    events: Vec<(&'a str, &'a str)>,
    show_email_popup: bool,
}







impl<'a> App<'a> {
    pub fn new(items: Vec<Email>) -> App<'a> {
        App {
            // TODO:
            account_titles: vec!["Fake Email 1", "Fake Email 2"],
            account_index: 0,
            items: StatefulList::with_items(items),
            events: vec![("New Mails", "INFO")],
            show_email_popup: false,
        }
    }

    pub fn next_account(&mut self) {
        self.account_index = (self.account_index + 1) % self.account_titles.len();
    }

    pub fn prev_account(&mut self) {
        if self.account_index > 0 {
            self.account_index -= 1;
        } else {
            self.account_index = self.account_titles.len() - 1;
        }
    }
}

pub fn start_tui(app: App) -> Result<(), Box<dyn Error>> {
    let mut terminal = setup_terminal()?;
    run(&mut terminal, app)?;
    restore_terminal(&mut terminal)?;
    Ok(())
}

fn setup_terminal() -> Result<Terminal<CrosstermBackend<Stdout>>, Box<dyn Error>> {
    let mut stdout = io::stdout();
    enable_raw_mode()?;
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    Ok(Terminal::new(CrosstermBackend::new(stdout))?)
}

fn restore_terminal(
    terminal: &mut Terminal<CrosstermBackend<Stdout>>,
) -> Result<(), Box<dyn Error>> {
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    Ok(terminal.show_cursor()?)
}

fn run(
    terminal: &mut Terminal<CrosstermBackend<Stdout>>,
    mut app: App,
) -> Result<(), Box<dyn Error>> {
    loop {
        terminal.draw(|frame| {
            ui(frame, &mut app);
        })?;
        if event::poll(Duration::from_millis(250))? {
            if let Event::Key(key) = event::read()? {
                if key.kind == KeyEventKind::Press {
                    match key.code {
                        KeyCode::Char('q') => return Ok(()),
                        KeyCode::Up => app.items.previous(),
                        KeyCode::Down => app.items.next(),
                        KeyCode::Left => app.items.unselect(),
                        KeyCode::Tab => app.next_account(),
                        KeyCode::BackTab => app.prev_account(),
                        KeyCode::Enter => app.show_email_popup = !app.show_email_popup,
                        _ => {}
                    }
                }
            }
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(1)
        .constraints(
            [
                Constraint::Percentage(6),
                Constraint::Percentage(10),
                Constraint::Percentage(80),
            ]
            .as_ref(),
        )
        .split(f.size());

    let key_hint_style = Style::default().fg(ratatui::style::Color::Cyan);
    let key_hint_text = String::from(format!(
        "{}: quit, {}: next selection, {}: previous selection. {}: view email",
        "q", "Up", "Down", "Enter"
    ));
    let key_hint = Span::styled(key_hint_text, key_hint_style);

    let block = Block::default().title(key_hint).borders(Borders::NONE);
    f.render_widget(block, chunks[0]);

    let tabs = app.account_titles.iter().cloned().map(Line::from).collect();
    let block_tabs = Block::default().borders(Borders::RIGHT);
    let style_tabs = Style::default().fg(ratatui::style::Color::Red);
    let email_tabs = Tabs::new(tabs).block(block_tabs).style(style_tabs);
    f.render_widget(email_tabs, chunks[1]);
    // TODO: Display inner content of tabs
    // let inner = match app.index {
    //     0 => Block::default().title("Inner 0").borders(Borders::ALL),
    //     1 => Block::default().title("Inner 1").borders(Borders::ALL),
    //     2 => Block::default().title("Inner 2").borders(Borders::ALL),
    //     3 => Block::default().title("Inner 3").borders(Borders::ALL),
    //     _ => unreachable!(),
    // };


    // TODO: Bundle account emails, title and index together instead of relying on separate
    // vectors with enforced ordering of items
    // for account in app.account_index {
    //
    // }

    let email_list = get_email_list_widget(&app.items.items);
    f.render_stateful_widget(email_list, chunks[2], &mut app.items.state);
}

fn get_email_list_widget<'a>(items: &Vec<Email>) -> List<'a> {
    let items: Vec<ListItem> = items
        .iter()
        .map(|email| ListItem::new(format!("{}", email)))
        .collect();
    let email_block = Block::default().title("Emails").borders(Borders::ALL);
    List::new(items)
        .block(email_block)
        .highlight_style(Style::default().add_modifier(Modifier::SLOW_BLINK))
        .highlight_symbol(">> ")
}
