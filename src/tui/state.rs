use statig::{
    state_machine, superstate,
    Response::{self, Handled, Super, Transition},
    StateOrSuperstate,
};

#[derive(Default)]
struct UIState {
    account_index: Option<usize>,
    email_index: Option<usize>,
}

// TODO:
enum UIEvent {
    TabChanged(usize),
    EmailDeselected,
    EmailSelected(usize),
    EmailPopupOpened(usize),
    EmailPopupClosed,
}

#[state_machine(
    initial = "State::account_deselected()",
    state(derive(Debug)),
    superstate(derive(Debug)),
    on_transition = "Self::on_state_transition",
    on_dispatch = "Self::on_state_dispatch"
)]
impl UIState {
    #[state]
    fn account_selected(&mut self, event: &UIEvent, index: &usize) -> Response<State> {
        match event {
            UIEvent::TabChanged(index) => {
                self.account_index = Some(*index);
                Handled
            }
            _ => Super,
        }
    }

    #[state]
    fn account_deselected(event: &UIEvent) -> Response<State> {
        match event {
            UIEvent::TabChanged(index) => Transition(State::account_selected(*index)),
            _ => Super,
        }
    }

    #[state]
    fn email_deselected(event: &UIEvent) -> Response<State> {
        match event {}
    }

    #[state]
    fn email_selected(event: &UIEvent) -> Response<State> {
        match event {}
    }

    #[state]
    fn email_popup_opened(event: &UIEvent) -> Response<State> {
        match event {}
    }

    #[state]
    fn email_popup_closed(event: &UIEvent) -> Response<State> {
        match event {}
    }
}

impl UIState {
    fn on_state_transition(&mut self, source: &State, target: &State) {}

    fn on_state_dispatch(&mut self, state: StateOrSuperstate<Self>, event: &UIEvent) {}
}
