use std::fmt::Display;

use diesel::prelude::*;


// TODO: Split insert model
#[derive(Queryable, Selectable, Insertable)]
#[diesel(table_name = crate::schema::emails)]
pub struct Email {
    pub id: Option<i32>,
    pub date: String,
    pub from: String,
    pub subject: String,
    pub body: String,
}

impl Display for Email {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} - {} - {}", self.date, self.from, self.subject)
    }
}
