use std::net::TcpStream;

use config::{Config, ConfigError};
use imap::Session;
use native_tls::TlsStream;
use serde_derive::Deserialize;

use crate::imap::ImapEmailAccount;

#[derive(Clone, Debug, Deserialize)]
pub struct EmailConfig {
    url: String,
    address: String,
    password: String,
}


#[derive(Debug, Deserialize)]
pub struct Settings {
    pub emails: Vec<EmailConfig>,
    // pub database_url: String,
}

pub fn get_config() -> Result<Settings, ConfigError> {
    let config = Config::builder()
        .add_source(config::File::with_name("env.toml"))
        // .add_source(config::File::with_name(".env"))
        .build()
        .unwrap();

    config.try_deserialize()
}

impl ImapEmailAccount for EmailConfig {
    fn get_account_session(&self) -> imap::error::Result<Session<TlsStream<TcpStream>>> {
    let domain: &str = &self.url;
    let tls = native_tls::TlsConnector::builder().build().unwrap();

    // we pass in the domain twice to check that the server's TLS
    // certificate is valid for the domain we're connecting to.
    let client = imap::connect((domain, 993), domain, &tls).unwrap();

    // the client we have here is unauthenticated.
    // to do anything useful with the e-mails, we need to log in
    client
        .login(&self.address, &self.password)
        .map_err(|e| e.0)
    }
}
