# Doggo

Doggo is an email client based on IMAP protocol and displaying contents in console through TUI and/or CLI queries.

## Architecture

Doggo uses layered architecture to separate concerns of different components and handle user input request flow.

Doggo's layeres are as follow:

### Presentation layer

Doggo uses TUI that is abstracted by trait IPresentation to display results to user.

### Business Layer

Business layer handles communication with remote IMAP-supporting email servers, handles internal details of emails like sorting and checking for doubles etc.

### Persistance layer

Handles relational mapping between internal models and database tables.

### Database Layer

Communicates with underlying database to store and retrieve data.
